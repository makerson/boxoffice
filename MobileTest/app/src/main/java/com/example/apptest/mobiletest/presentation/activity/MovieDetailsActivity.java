package com.example.apptest.mobiletest.presentation.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.apptest.mobiletest.Data.request.MovieDetailsTask;
import com.example.apptest.mobiletest.Data.response.MovieResponse;
import com.example.apptest.mobiletest.R;
import com.example.apptest.mobiletest.callback.MovieDetailsListener;
import com.example.apptest.mobiletest.persistance.db.RetrieveReview;
import com.example.apptest.mobiletest.persistance.db.SaveReview;
import com.example.apptest.mobiletest.persistance.entity.MyReviewEntity;
import com.squareup.picasso.Picasso;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MovieDetailsActivity extends BaseActivity implements MovieDetailsListener {


    public static final String IM_DB_ID = "imDbID";

    @BindDrawable(R.drawable.ic_remove_from_queue_black_44dp) Drawable appLogo;

    @BindView(R.id.details_title) TextView titleView;
    @BindView(R.id.details_release_date) TextView releaseDateView;
    @BindView(R.id.details_poster) ImageView posterView;
    @BindView(R.id.details_rating_critics) RatingBar ratingBarCritics;
    @BindView(R.id.details_rating_audience) RatingBar ratingBarAudience;
    @BindView(R.id.details_my_rating) RatingBar myRating;
    @BindView(R.id.details_my_review_text) TextView myReviewText;
    @BindView(R.id.details_plot) TextView synopsisView;
    @BindView(R.id.details_review_label) TextView reviewLabel;
    @BindView(R.id.details_synopsis_label) TextView synopsisLabel;



    @BindString(R.string.edit_review_dialog_title) String dialogTitle;
    @BindString(R.string.release_date_text) String dateStringFormatter;
    @BindString(R.string.dialog_cancel) String dialogCancelText;
    @BindString(R.string.dialog_save) String dialogSaveText;

    private String imDbID;
    private float ratingValue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setLogo(appLogo);
        }

        reviewLabel.setPaintFlags(reviewLabel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        synopsisLabel.setPaintFlags(synopsisLabel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if (getIntent() != null) {
            imDbID = getIntent().getStringExtra(IM_DB_ID);
            if(isInternetOn()){
                new RetrieveReview(getBaseContext(), this).execute(imDbID);
                new MovieDetailsTask(this).execute(imDbID);
            }

        }

    }

    @Override
    public void onMovieDetailsAvailable(MovieResponse movie) {
        titleView.setText(movie.getTitle());
        releaseDateView.setText(String.format(dateStringFormatter, movie.getReleased()));
        synopsisView.setText(movie.getPlot());
        float criticsRate = Float.parseFloat(movie.getMetascore());
        ratingBarCritics.setRating(criticsRate * ratingBarCritics.getNumStars() / 100);
        float audienceRate = Float.parseFloat(movie.getImdbRating());
        ratingBarAudience.setRating(audienceRate * ratingBarCritics.getNumStars() / 10);
        Picasso.with(posterView.getContext()).load(movie.getPoster()).into(posterView);
    }

    @Override
    public void onReviewRetrieve(MyReviewEntity myReviewEntity) {
        myRating.setRating(myReviewEntity.getRatingValue());
        myReviewText.setText(myReviewEntity.getReviewContent());
    }


    @OnClick(R.id.container_review)
    public void showEditDialog(){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        dialogBuilder.setTitle(dialogTitle);

        RatingBar dialogRatingBar = (RatingBar) dialogView.findViewById(R.id.dialog_rating_bar);
        final EditText  dialogEditText = (EditText) dialogView.findViewById(R.id.dialog_review_text);

        dialogBuilder.setPositiveButton(dialogSaveText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                myReviewText.setText(dialogEditText.getText());
                myRating.setRating(ratingValue);
                new SaveReview(getBaseContext(),
                        new MyReviewEntity(imDbID,ratingValue,
                                myReviewText.getText().toString())).execute(imDbID);
                ratingValue = 0f;
                dialog.dismiss();
            }
        });
        dialogBuilder.setNegativeButton(dialogCancelText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ratingValue = 0f;
                dialog.dismiss();
            }
        });

        dialogEditText.setText(myReviewText.getText());

        dialogRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingValue = rating;

            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();

    }
}
