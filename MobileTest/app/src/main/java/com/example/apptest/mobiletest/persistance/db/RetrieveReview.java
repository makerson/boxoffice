package com.example.apptest.mobiletest.persistance.db;

import android.content.Context;
import android.os.AsyncTask;

import com.example.apptest.mobiletest.callback.MovieDetailsListener;
import com.example.apptest.mobiletest.persistance.entity.MyReviewEntity;

import java.lang.ref.WeakReference;

/**
 * Created by pierremakersonchery on 28/01/2018.
 */

public class RetrieveReview extends AsyncTask<String, Void, MyReviewEntity> {

    private final WeakReference<Context> contextReference;
    private MovieDetailsListener movieDetailsListener;


    public RetrieveReview(Context baseContext, MovieDetailsListener movieDetailsListener) {
        this.contextReference = new WeakReference<>(baseContext);
        this.movieDetailsListener = movieDetailsListener;
    }

    @Override
    protected MyReviewEntity doInBackground(String... params) {
        final Context context = contextReference.get();
        String iMDBId = params[0];
        MyReviewEntity myReviewEntity = null;
        if (context != null) {
            myReviewEntity = movieDB.getInstance(context)
                    .getReviewDao()
                    .getMyReviewForImDbId(iMDBId);
        }

        return myReviewEntity;
    }

    @Override
    protected void onPostExecute(MyReviewEntity myReviewEntity) {
        super.onPostExecute(myReviewEntity);
        if (movieDetailsListener != null && myReviewEntity!= null) {
            movieDetailsListener.onReviewRetrieve(myReviewEntity);
        }
    }
}