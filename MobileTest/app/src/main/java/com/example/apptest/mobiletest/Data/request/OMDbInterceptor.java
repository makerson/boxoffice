package com.example.apptest.mobiletest.Data.request;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by pierremakersonchery on 27/01/2018.
 */

public class OMDbInterceptor implements Interceptor {
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        final Request request = chain.request();
        HttpUrl httpUrlRequest = request.url();

        HttpUrl url = httpUrlRequest.newBuilder()
                .addQueryParameter("apikey", "121ae6ad")
                .build();

        // Request customization: add request headers
        Request.Builder requestBuilder = request.newBuilder()
                .url(url);

        Request newRequest = requestBuilder.build();

        return chain.proceed(newRequest);
    }
}
