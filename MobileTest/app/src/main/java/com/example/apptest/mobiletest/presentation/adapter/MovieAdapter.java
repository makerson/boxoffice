package com.example.apptest.mobiletest.presentation.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apptest.mobiletest.Data.response.MovieResponse;
import com.example.apptest.mobiletest.R;
import com.example.apptest.mobiletest.presentation.adapter.listener.OnMovieClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pierremakersonchery on 27/01/2018.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> implements Filterable {

    private final List<MovieResponse> mListMovie;
    private List<MovieResponse> mListFilteredMovie;
    private final OnMovieClickListener mListener;
    private CustomFilter mFilter;
    private int[] listItemBackground;

    public MovieAdapter(List<MovieResponse> MovieList, OnMovieClickListener listener) {
        this.mListMovie = MovieList;
        this.mListFilteredMovie = MovieList;
        mListener = listener;
        listItemBackground = new int[] {R.color.white,
                R.color.green_bright};
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.movieItem = mListFilteredMovie.get(position);
        Picasso.with(holder.mImageView.getContext()).load(holder.movieItem.getPoster()).into(holder.mImageView);

        holder.mTitleView.setText(holder.movieItem.getTitle());
        holder.mYearView.setText(String.format(holder.itemView.getContext().getString(R.string.movie_year), holder.movieItem.getYear()));
        holder.mTypeView.setText(String.format(holder.itemView.getContext().getString(R.string.movie_type), holder.movieItem.getType()));


        int listItemBackgroundPosition = position % listItemBackground.length;
        holder.mCardView.setCardBackgroundColor(ContextCompat.getColor(holder.mCardView.getContext(),listItemBackground[listItemBackgroundPosition]));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onMovieClick(holder.movieItem.getImdbID());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListFilteredMovie.size();
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null)
            mFilter = new CustomFilter();

        return mFilter;
    }


    private class CustomFilter extends Filter {
        private CustomFilter() {
            super();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String charString = constraint.toString();
            final FilterResults results = new FilterResults();


            if (charString.isEmpty()) {
                mListFilteredMovie = mListMovie;
            } else {
                ArrayList<MovieResponse> filterList = new ArrayList<>();
                for (MovieResponse movieItem : mListMovie) {
                    if (movieItem.getTitle().toLowerCase().contains(charString.toLowerCase()) ||
                            movieItem.getYear().toLowerCase().contains(charString.toLowerCase()) ||
                            movieItem.getType().toLowerCase().contains(charString.toLowerCase())) {
                        filterList.add(movieItem);
                    }
                }
                mListFilteredMovie = filterList;
            }

            results.values = mListFilteredMovie;
            results.count = mListFilteredMovie.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mListFilteredMovie = (List<MovieResponse>) results.values;
            notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final CardView mCardView;
        private final ImageView mImageView;
        private final TextView mTitleView;
        private final TextView mYearView;
        private final TextView mTypeView;
        private MovieResponse movieItem;

        ViewHolder(View view) {
            super(view);
            mImageView = view.findViewById(R.id.poster);
            mTitleView = view.findViewById(R.id.title);
            mYearView = view.findViewById(R.id.year);
            mTypeView = view.findViewById(R.id.type);
            mCardView = view.findViewById(R.id.card_view_item);
        }

    }


}
