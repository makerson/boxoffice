package com.example.apptest.mobiletest.Data.request;

import android.os.AsyncTask;

import com.example.apptest.mobiletest.Data.response.MovieResponse;
import com.example.apptest.mobiletest.callback.MovieDetailsListener;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pierremakersonchery on 27/01/2018.
 */

public class MovieDetailsTask extends AsyncTask<String,Void,MovieResponse> {

    private MovieDetailsListener movieDetailsListener;

    public MovieDetailsTask(MovieDetailsListener searchMovieListener) {
        this.movieDetailsListener = searchMovieListener;
    }

    @Override
    protected MovieResponse doInBackground(String...params) {

        MovieResponse movieResponse = null;

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        httpClient.addInterceptor(new OMDbInterceptor() );
        OkHttpClient okHttpClient = httpClient.build();

        OMDbApi omdbApi = new Retrofit.Builder()
                .baseUrl(OMDbApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(OMDbApi.class);

        String iMDBId = params[0];

        try {
            movieResponse = omdbApi.getMovieByIMDBId(iMDBId).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return movieResponse;
    }

    @Override
    protected void onPostExecute(MovieResponse movie) {
        super.onPostExecute(movie);
        if(movieDetailsListener!= null){
            movieDetailsListener.onMovieDetailsAvailable(movie);
        }
    }
}