package com.example.apptest.mobiletest.presentation.adapter.listener;

/**
 * Created by pierremakersonchery on 27/01/2018.
 */

public interface OnMovieClickListener {
    void onMovieClick(String iMDbID);
}
