package com.example.apptest.mobiletest.Data.request;

import com.example.apptest.mobiletest.Data.response.MovieResponse;
import com.example.apptest.mobiletest.Data.response.SearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by pierremakersonchery on 27/01/2018.
 */

public interface OMDbApi {
    String BASE_URL = "http://www.omdbapi.com";

    @GET("/")
    Call<SearchResponse> searchMovieByTitle(@Query("s") String movieTitle);

    @GET("/")
    Call<MovieResponse> getMovieByIMDBId(@Query("i") String movieId);

}