package com.example.apptest.mobiletest.persistance.db;

import android.content.Context;
import android.os.AsyncTask;

import com.example.apptest.mobiletest.persistance.entity.MyReviewEntity;

import java.lang.ref.WeakReference;

/**
 * Created by pierremakersonchery on 28/01/2018.
 */

public class SaveReview extends AsyncTask<String, Void, Long> {

    private MyReviewEntity myReviewEntity;
    private final WeakReference<Context> contextReference;


    public SaveReview(Context baseContext, MyReviewEntity myReviewEntity) {
        this.contextReference = new WeakReference<>(baseContext);
        this.myReviewEntity = myReviewEntity;
    }


    @Override
    protected Long doInBackground(String... params) {
        Long insertId = -1L;
        final Context context = contextReference.get();
        if (context != null) {
            String iMDBId = params[0];
            MyReviewEntity reviewEntity = movieDB.getInstance(context)
                    .getReviewDao()
                    .getMyReviewForImDbId(iMDBId);
            if (reviewEntity != null) {
                reviewEntity.setReviewContent(myReviewEntity.getReviewContent());
                reviewEntity.setRatingValue(myReviewEntity.getRatingValue());
                insertId = (long) movieDB.getInstance(context)
                        .getReviewDao()
                        .updateMyReview(reviewEntity);
            } else {
                insertId = movieDB.getInstance(context)
                        .getReviewDao()
                        .insertMyReview(myReviewEntity);
            }

        }


        return insertId;
    }

    @Override
    protected void onPostExecute(Long insertId) {
        super.onPostExecute(insertId);
    }
}