package com.example.apptest.mobiletest.persistance.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by pierremakersonchery on 27/01/2018.
 */
@Entity
public class MyReviewEntity {

    @PrimaryKey(autoGenerate = true)
    private Long reviewId;
    private String imDbId;
    private float ratingValue;
    private String reviewContent;

    public MyReviewEntity(String imDbId, float ratingValue , String reviewContent) {
        this.imDbId = imDbId;
        this.ratingValue = ratingValue;
        this.reviewContent = reviewContent;
    }

    public Long getReviewId() {
        return reviewId;
    }

    public void setReviewId(Long reviewId) {
        this.reviewId = reviewId;
    }

    public String getImDbId() {
        return imDbId;
    }

    public void setImDbId(String imDbId) {
        this.imDbId = imDbId;
    }

    public float getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(float ratingValue) {
        this.ratingValue = ratingValue;
    }

    public String getReviewContent() {
        return reviewContent;
    }

    public void setReviewContent(String reviewContent) {
        this.reviewContent = reviewContent;
    }
}
