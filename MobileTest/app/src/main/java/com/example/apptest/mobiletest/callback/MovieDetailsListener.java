package com.example.apptest.mobiletest.callback;

import com.example.apptest.mobiletest.Data.response.MovieResponse;
import com.example.apptest.mobiletest.persistance.entity.MyReviewEntity;

/**
 * Created by pierremakersonchery on 27/01/2018.
 */

public interface MovieDetailsListener {
    void onMovieDetailsAvailable(MovieResponse movie);
    void onReviewRetrieve(MyReviewEntity myReviewEntity);
}
