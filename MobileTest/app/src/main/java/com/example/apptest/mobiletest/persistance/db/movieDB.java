package com.example.apptest.mobiletest.persistance.db;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.apptest.mobiletest.persistance.dao.ReviewDao;
import com.example.apptest.mobiletest.persistance.entity.MyReviewEntity;

/**
 * Created by pierremakersonchery on 28/01/2018.
 */
@Database(entities = { MyReviewEntity.class}, version = 1)
public abstract class movieDB extends RoomDatabase {

    private static final String DB_NAME = "movieDatabase.db";
    private static volatile movieDB instance;

   public static synchronized movieDB getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }


    private static movieDB create(final Context context) {
        return Room.databaseBuilder(
                context,
                movieDB.class,
                DB_NAME).build();
    }



    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    public abstract ReviewDao getReviewDao();
}
