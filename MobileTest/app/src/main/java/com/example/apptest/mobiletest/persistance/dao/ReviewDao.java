package com.example.apptest.mobiletest.persistance.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.apptest.mobiletest.persistance.entity.MyReviewEntity;

/**
 * Created by pierremakersonchery on 28/01/2018.
 */
@Dao
public interface ReviewDao {

    @Query("SELECT * from MyReviewEntity WHERE imDbId = :imDbId")
    MyReviewEntity getMyReviewForImDbId(String imDbId);

    @Insert
    Long insertMyReview(MyReviewEntity reviewEntity);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateMyReview(MyReviewEntity reviewEntity);


}
