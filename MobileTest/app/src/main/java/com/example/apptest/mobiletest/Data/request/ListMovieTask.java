package com.example.apptest.mobiletest.Data.request;

import android.os.AsyncTask;

import com.example.apptest.mobiletest.Data.response.MovieResponse;
import com.example.apptest.mobiletest.Data.response.SearchResponse;
import com.example.apptest.mobiletest.callback.SearchMovieListener;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pierremakersonchery on 27/01/2018.
 */

public class ListMovieTask extends AsyncTask<String,Void,List<MovieResponse>> {

    private SearchMovieListener searchMovieListener;

    public ListMovieTask(SearchMovieListener searchMovieListener) {
        this.searchMovieListener = searchMovieListener;
    }

    @Override
    protected List<MovieResponse> doInBackground(String...params) {

        List<MovieResponse> movieList = null;

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        httpClient.addInterceptor(new OMDbInterceptor() );
        OkHttpClient okHttpClient = httpClient.build();

        OMDbApi omdbApi = new Retrofit.Builder()
                .baseUrl(OMDbApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(OMDbApi.class);

        String movieTitle = params[0];

        try {
            SearchResponse searchResponse = omdbApi.searchMovieByTitle(movieTitle).execute().body();
            movieList = searchResponse != null ? searchResponse.getListMovie() : null;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return movieList;
    }

    @Override
    protected void onPostExecute(List<MovieResponse> movies) {
        super.onPostExecute(movies);
        if(searchMovieListener!= null){
            searchMovieListener.onListMovieAvailable(movies);
        }
    }
}