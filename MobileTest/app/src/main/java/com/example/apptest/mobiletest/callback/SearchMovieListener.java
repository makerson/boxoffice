package com.example.apptest.mobiletest.callback;

import com.example.apptest.mobiletest.Data.response.MovieResponse;

import java.util.List;

/**
 * Created by pierremakersonchery on 27/01/2018.
 */

public interface SearchMovieListener {
    void onListMovieAvailable(List<MovieResponse> movieResponseList);
}
