package com.example.apptest.mobiletest.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.apptest.mobiletest.R;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by pierremakersonchery on 10/01/2018.
 */

public class ConnectionErrorActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindString(R.string.app_error_label) String errorLabel;
    @BindDrawable(R.drawable.ic_remove_from_queue_black_44dp) Drawable appLogo;
    @BindString(R.string.error_details) String errorExplanation;


    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connection_error);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(errorLabel);
            getSupportActionBar().setLogo(appLogo);
        }

    }

    @OnClick(R.id.btn_retry)
    public void recheckConnection(){
        if (!isInternetOn()) {
            showSnackMessage(errorExplanation, Color.YELLOW);
        }else {
            Intent intent = new Intent(ConnectionErrorActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    private void showSnackMessage(String message, int colorValue) {
        CoordinatorLayout layout = findViewById(R.id.coordinatorLay);
        Snackbar snackbar = Snackbar.make(layout, message, Snackbar.LENGTH_LONG);
        // Changing message text color
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(colorValue);
        snackbar.show();
    }

    private  boolean isInternetOn() {

        NetworkInfo networkInfo = null;
        boolean isConnected = true;

        // get Connectivity Manager object to check connection
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }

        if (networkInfo == null || !networkInfo.isConnected() ||
                (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                        && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
            // If no connectivity, cancel task and update Callback with null data.
            isConnected = false;
        }
        return isConnected;
    }


}
