package com.example.apptest.mobiletest.Data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by pierremakersonchery on 27/01/2018.
 */

public class SearchResponse {

    @SerializedName("Search")
    @Expose
    private List<MovieResponse> movieList = null;
    @SerializedName("totalResults")
    @Expose
    private String totalResults;
    @SerializedName("Response")
    @Expose
    private String response;

    public List<MovieResponse> getListMovie() {
        return movieList;
    }

    public void setListMovie(List<MovieResponse> searchContentList) {
        this.movieList = searchContentList;
    }

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}