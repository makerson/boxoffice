package com.example.apptest.mobiletest.presentation.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.apptest.mobiletest.Data.request.ListMovieTask;
import com.example.apptest.mobiletest.Data.response.MovieResponse;
import com.example.apptest.mobiletest.R;
import com.example.apptest.mobiletest.callback.SearchMovieListener;
import com.example.apptest.mobiletest.presentation.adapter.MovieAdapter;
import com.example.apptest.mobiletest.presentation.adapter.listener.OnMovieClickListener;
import com.example.apptest.mobiletest.presentation.view.EmptyRecyclerView;

import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements SearchMovieListener, OnMovieClickListener, SearchView.OnQueryTextListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.search_progress) View mProgressView;
    @BindView(R.id.container_movie) View mMovieContainer;
    @BindView(R.id.my_recycler_view) EmptyRecyclerView recyclerView;
    @BindView(R.id.movie_list_empty_view) TextView emptyView;




    @BindString(R.string.no_result_found) String noResultText;
    @BindDrawable(R.drawable.ic_remove_from_queue_black_44dp) Drawable appLogo;

    private MovieAdapter movieAdapter;
    private SearchView searchView;
    private MenuItem searchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setLogo(appLogo);
        }

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(mLayoutManager);

        // Fetch the empty view from the layout and set it on
        // the new recycler view
        emptyView.setText(noResultText);
        recyclerView.setEmptyView(emptyView);

        if (isInternetOn()) {
            showProgress(true);
            new ListMovieTask(this).execute("Batman");
        }
    }

    @Override
    public void onListMovieAvailable(List<MovieResponse> movieResponseList) {
        showProgress(false);
        movieAdapter = new MovieAdapter(movieResponseList, this);
        recyclerView.setAdapter(movieAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();
        if (searchManager != null) {
            searchView.setSearchableInfo(searchManager
                    .getSearchableInfo(getComponentName()));
        }

        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        } else if (id == R.id.action_refresh) {
            showProgress(true);
            new ListMovieTask(this).execute("Batman");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMovieClick(String iMDdID) {

        // close search view if its visible
        if (searchView != null && searchView.isShown()) {
            searchMenuItem.collapseActionView();
            searchView.setQuery("", false);
        }

        Intent intent = new Intent(this, MovieDetailsActivity.class);
        intent.putExtra(MovieDetailsActivity.IM_DB_ID, iMDdID);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (movieAdapter != null && searchView != null) {
            searchView.setVisibility(View.VISIBLE);
            movieAdapter.getFilter().filter(newText);
        }
        return false;
    }


    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mMovieContainer.setVisibility(show ? View.GONE : View.VISIBLE);
        mMovieContainer.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mMovieContainer.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });


        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });

    }
}
